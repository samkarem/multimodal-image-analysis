#######
IMPLEMENTING MULTIMODAL DENSENET:
Author: Sam Sukeerth Karem
#######
Dataset:

Research Paper is found [here](https://samkarem.gitlab.io/home/MMBA.pdf)
All the images for US, MLO, and CC modalities are present in the '../dataset' folder.


These images were converted into numpy format from dicom format using the 'Segmentation.ipynb' jupyter notebook. The code in the notebook should be self explantory with necessary comments.


The pathology report for the patients is found in the mamo_patients.csv file. This is mapped with the dataset in the code for running models.


Some Images of the dataset can also be found in the '../Dataset\ Images' folder


UNI MODAL:


Unimodal code for CC is found in demoCC.py. It's implemented using the shell script runcc.sh. The Hyperparameters of the model can be modified in the script rather than modifying them in the code


Unimodal code for MLO is found in demoMlo.py. It's implemented using the shell script runcc.sh. The Hyperparameters of the model can be modified in the script rather than modifying them in the code


Unimodal code for US is found in demoUS.py. It's implemented using the shell script runcc.sh. The Hyperparameters of the model can be modified in the script rather than modifying them in the code


MULTI_MODAL:


The multimodal densenet named 'Condensenet' is found in the './models' folder.


This model is used is running the multimodal code found in demo.py.


It's implemented using the sheel script run.sh. Same as the Unimodal hyperparameters can be modified here.
