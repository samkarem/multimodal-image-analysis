#!/bin/bash

# Configure the resources required
#SBATCH -n 1                                                    # number of cores (here uses 8, up to 32 cores are permitted)
#SBATCH --time=13:59:00                                         # time allocation, which has the format (D-HH:MM), here set to 1 hour
#SBATCH --gres=gpu:4                                            # generic resource required (here requires 4 gpu cores)
#SBATCH --mem=32GB                                              # memory pool for all cores (here set to 16 GB)


#BATCH --mail-type=END                          # Type of email notifications will be sent (here set to END, which means an email will be sent when the job is done)
#SBATCH --mail-type=FAIL                         # Type of email notifications will be sent (here set to FAIL, which means an email will be sent when the job is fail to complete)
#SBATCH --mail-user=samsukeerth.karem@student.adelaide.edu.au     # Email to which notification will be sent


module load cuDNN
module load Python/3.6.1-foss-2016b


cd /fast/users/a1760301/
source mmba_env/bin/activate


cd /fast/users/a1760301/MMBA/scripts

python demoUS.py --data ../dataset/ --save ./results_us_e3 --lr 0.001 --wd 0.0 --shuffle True --efficient True --dropout 0.0 --random_crop True --n_epochs 50
deactivate
