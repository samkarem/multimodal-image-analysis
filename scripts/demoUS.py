import fire as fire
import os
import time
import random
import pandas as pd
import torch
from sklearn import metrics
import numpy as np
import csv
import torch.nn as nn
from skimage import exposure
from skimage.morphology import disk
from skimage.filters import rank
import torchvision.models as models
import torch.utils.data as data
from torchvision import datasets, transforms
#from models import DenseNet
from models import linear
import torch.nn.functional as F

model = []
class AverageMeter(object):
    """
    Computes and stores the average and current value
    Copied from: https://github.com/pytorch/examples/blob/master/imagenet/main.py
    """
    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count

def train_epoch(model, loader, optimizer, epoch, n_epochs, print_freq=1):
    batch_time = AverageMeter()
    losses = AverageMeter()
    error = AverageMeter()
    accuracy = AverageMeter()
    ROC = AverageMeter()

    # Model on train mode
    model.train()

    end = time.time()
    for batch_idx, (input1, target) in enumerate(loader):
        if torch.cuda.is_available():
            weights=torch.tensor([float(len(target))/float(sum(target)+1),float(len(target))/float((len(target)-sum(target)+1))]).cuda()
        else:
            weights=torch.tensor([float(len(target))/float(sum(target)+1),float(len(target))/float((len(target)-sum(target)+1))]).cpu()

        # Create vaiables
        if torch.cuda.is_available():
            input1 = input1.cuda()
            target = target.cuda()
        else:
            input1 = input1.cpu()
            target = target.cpu()
        # compute output
        output = model(input1)
        #print("Output: ",output)
        softmax=nn.Softmax(1)
        probDist=softmax(output)
        probDist=probDist.data.cpu().numpy()
        y_scores=[i[1] for i in probDist]
        newTargets=target.cpu().numpy()

        loss = torch.nn.functional.cross_entropy(output, target,  weight=weights)

        # measure accuracy and record loss
        batch_size = target.size(0)
        _, pred = output.data.cpu().topk(1, dim=1)
        if torch.cuda.is_available():
            accuracy.update(metrics.accuracy_score(target.cuda(), pred.cuda()))
        else:
            accuracy.update(metrics.accuracy_score(target.cpu(), pred.cpu()))
        # fpr, tpr, thresholds = metrics.roc_curve(newTargets, y_scores)
        # ROC.update(metrics.auc(fpr, tpr))
        # try:
        #print('yscores \n',y_scores)
        #print('targets \n',newTargets)

        ROC.update(metrics.roc_auc_score(newTargets, y_scores))
        # except:
            # ROC.update(0)

        error.update(torch.ne(pred.squeeze(), target.cpu()).float().sum().item() / batch_size, batch_size)
        losses.update(loss.item(), batch_size)

        # compute gradient and do SGD step
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        # measure elapsed time
        batch_time.update(time.time() - end)
        end = time.time()

        # print stats
        if batch_idx % print_freq == 0:
            res = '\t'.join([
                'Epoch: [%d/%d]' % (epoch + 1, n_epochs),
                'Iter: [%d/%d]' % (batch_idx + 1, len(loader)),
                'Time %.3f (%.3f)' % (batch_time.val, batch_time.avg),
                'Loss %.4f (%.4f)' % (losses.val, losses.avg),
                'Error %.4f (%.4f)' % (error.val, error.avg),
            ])
            print(res)

    # Return summary statistics
    return batch_time.avg, losses.avg, error.avg, accuracy.avg, ROC.avg


def test_epoch(model, loader, print_freq=1, is_test=True):
    batch_time = AverageMeter()
    losses = AverageMeter()
    error = AverageMeter()
    accuracy = AverageMeter()
    ROC = AverageMeter()

    # Model on eval mode
    model.eval()

    end = time.time()
    with torch.no_grad():
        for batch_idx, (input1, target) in enumerate(loader):
            if torch.cuda.is_available():
                weights=torch.tensor([float(len(target))/float(sum(target)+1),float(len(target))/float((len(target)-sum(target)+1))]).cuda()
            else:
                weights=torch.tensor([float(len(target))/float(sum(target)+1),float(len(target))/float((len(target)-sum(target)+1))]).cpu()

            # Create vaiables
            if torch.cuda.is_available():
                input1 = input1.cuda()
                target = target.cuda()
            else:
                input1 = input1.cpu()
                target = target.cpu()

            # compute output
            output = model(input1)

            softmax=nn.Softmax(1)
            probDist=softmax(output)
            probDist=probDist.data.cpu().numpy()
            y_scores=[i[1] for i in probDist]
            newTargets=target.cpu().numpy()

            loss = torch.nn.functional.cross_entropy(output, target, weight=weights)

            # measure accuracy and record loss
            batch_size = target.size(0)
            _, pred = output.data.cpu().topk(1, dim=1)
            print("loss"+str(loss.item()))
            print("batchloss"+str(loss.item()*batch_size))
            if torch.cuda.is_available():
                accuracy.update(metrics.accuracy_score(target.cuda(), pred.cuda()))
            else:
                accuracy.update(metrics.accuracy_score(target.cpu(), pred.cpu()))
            # fpr, tpr, thresholds = metrics.roc_curve(target.cpu(), pred.cpu())
            # ROC=metrics.auc(fpr, tpr)
            try:
                if(is_test):
                    print('Test yscores \n',y_scores)
                    print('Test targets \n',newTargets)
                else:
                    print('Validation yscores \n',y_scores)
                    print('Validation targets \n',newTargets)
            except:
                print("Could not prints scores")

            ROC.update(metrics.roc_auc_score(newTargets, y_scores))
            # except:
            #     ROC.update(0)

            error.update(torch.ne(pred.squeeze(), target.cpu()).float().sum().item() / batch_size, batch_size)
            losses.update(loss.item(), batch_size)

            # measure elapsed time
            batch_time.update(time.time() - end)
            end = time.time()

            # print stats
            if batch_idx % print_freq == 0:
                res = '\t'.join([
                    'Test' if is_test else 'Valid',
                    'Iter: [%d/%d]' % (batch_idx + 1, len(loader)),
                    'Time %.3f (%.3f)' % (batch_time.val, batch_time.avg),
                    'Loss %.4f (%.4f)' % (losses.val, losses.avg),
                    'Error %.4f (%.4f)' % (error.val, error.avg),
                ])
                print(res)

    # Return summary statistics
    return batch_time.avg, losses.avg, error.avg, accuracy.avg, ROC.avg


def train(model, train_set, valid_set, test_set, save, n_epochs,
          batch_size, lr, wd, shuffle, momentum=0.9, seed=None):
    if seed is not None:
        torch.manual_seed(seed)

    # Data loaders
    train_loader = torch.utils.data.DataLoader(train_set, batch_size=batch_size, shuffle=shuffle,
                                               pin_memory=(torch.cuda.is_available()), num_workers=0)
    test_loader = torch.utils.data.DataLoader(test_set, batch_size=len(test_set), shuffle=shuffle,
                                              pin_memory=(torch.cuda.is_available()), num_workers=0)
    if valid_set is None:
        valid_loader = None
    else:
        valid_loader = torch.utils.data.DataLoader(valid_set,  batch_size=len(valid_set), shuffle=shuffle, pin_memory=(torch.cuda.is_available()), num_workers=0)
    # Model on cuda
    if torch.cuda.is_available():
        model = model.cuda()
    else:
        model = model.cpu()
    # Wrap model for multi-GPUs, if necessary
    model_wrapper = model
    if torch.cuda.is_available() and torch.cuda.device_count() > 1:
        model_wrapper = torch.nn.DataParallel(model).cuda()
    #print(model_wrapper)
    # Optimizer
    optimizer = torch.optim.Adam(model_wrapper.parameters(), lr=lr, weight_decay=wd)
   # optimizer = torch.optim.SGD(model_wrapper.parameters(), lr=lr, momentum=momentum, nesterov=True, weight_decay=wd)
    scheduler = torch.optim.lr_scheduler.MultiStepLR(optimizer, milestones=[0.5 * n_epochs, 0.75 * n_epochs],
                                                     gamma=0.1)

    # Start log
    with open(os.path.join(str(save), 'results.csv'), 'w') as f:
        f.write('epoch,train_loss,train_error,valid_loss,valid_error,train_acc,test_acc,train_roc,valid_roc,test_error,test_roc\n')

    # Train model
    best_error = 1
    for epoch in range(n_epochs):
        print('Epoch: ', epoch, '/',n_epochs)
        #scheduler.step()
        _, train_loss, train_error,train_acc,train_roc = train_epoch(
            model=model_wrapper,
            loader=train_loader,
            optimizer=optimizer,
            epoch=epoch,
            n_epochs=n_epochs,
        )
        #scheduler.step()
        _, valid_loss, valid_error,valid_acc,valid_roc = test_epoch(
            model=model_wrapper,
            loader=valid_loader if valid_loader else test_loader,
            is_test=False
        )
        print('Valid ROC: ', valid_roc)

        # Determine if model is the best
        if valid_loader and valid_error < best_error:
            best_error = valid_error
            print('New best error: %.4f' % best_error)
            torch.save(model.state_dict(), os.path.join(save, 'model.dat'))
        else:
            torch.save(model.state_dict(), os.path.join(save, 'model.dat'))
        _, _, test_error, _, test_roc = test_epoch(model=model, loader=test_loader, is_test=True)
        # Log results
        scheduler.step()
        with open(os.path.join(str(save), 'results.csv'), 'a') as f:
            f.write('%03d,%0.6f,%0.6f,%0.5f,%0.5f,%0.5f,%0.5f,%0.5f,%0.5f,%0.5f,%0.5f\n' % (
                (epoch + 1),
                train_loss,
                train_error,
                valid_loss,
                valid_error,
                train_acc,
                valid_acc,
                train_roc,
                valid_roc,
                test_error,
                test_roc
            ))

    # Final test of model on test set
    model.load_state_dict(torch.load(os.path.join(save, 'model.dat')))
    if torch.cuda.is_available() and torch.cuda.device_count() > 1:
        model = torch.nn.DataParallel(model).cuda()
    test_results = test_epoch(
        model=model,
        loader=test_loader,
        is_test=True
    )
    _, _, test_error, _, _ = test_results
    with open(os.path.join(str(save), 'results.csv'), 'a') as f:
        f.write('%0.5f\n' % (test_error))
    print('Final test error: %.4f' % test_error)

def GetClass(value):
    if value == '0':
        return 0
    elif value == '1':
        return 0
    else:
        return 1

def LoadBiRaids(csv_file, view):

    with open(csv_file) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        entries = []
        counter=0

        if view:
            notfound = ['','437878','617028','273095','601276','726059','675635','1041244','157264','234361','136500','148935','TV_1','Patient_ID','587348','63392','530009','707347','607376','850517','337898','861280','773709','204110','21875','278342','731809','661844','199244','83988','968408','630362','32448','165954','1041821','173035','370123','108136','375522']
        else:
            notfound = ['','707347','1041244','530009','168603','148935','TV_1','Patient_ID','587348','63392','530009','707347','607376','850517','337898','861280','773709','204110','21875','278342','731809','661844','199244','83988','968408','630362','32448','165954','1041821','370123','108136','375522','25262']
        for row in csv_reader:
            counter+=1

            if counter <= 3 or row[0] in notfound:
                continue
            else:
                if 'x' in row[11] or 'x'in row[12] or 'x' in row[13] or 'x' in row[14] or 'x' in row[10]:
                    continue

                if '?' in row[11] or '?'in row[12] or '?' in row[13] or '?' in row[14] or '?' in row[10]:
                    continue
                if (view):
                    if row[10] and row[12] and row[14]:
                        entries.append({"name": row[0], "score": GetClass(max(row[10],row[12],row[14])),"side":"left","us":1})
                    elif row[10] and row[12]:
                        entries.append({"name": row[0], "score": GetClass(max(row[10],row[12])),"side":"left","us":0})
                else:
                    if row[11] and row[13] and row[15]:
                        entries.append({"name": row[0], "score": GetClass(max(row[11],row[13],row[15])),"side":"right","us":1})
                    elif row[11] and row[13]:
                        entries.append({"name": row[0], "score": GetClass(max(row[11],row[13])),"side":"right","us":0})


    return entries

class LoadDataset(data.Dataset):
    """Face Landmarks dataset."""

    def __init__(self, biraids, transform=None):

        self.Biraids = biraids
        self.root_dir = os.path.abspath(os.path.join(os.path.abspath(__file__), '../..'))
        self.transform = transform

    def __len__(self):
        return len(self.Biraids)

    def __getitem__(self, idx):

        img_us = os.path.join(self.root_dir, "dataset/"+ self.Biraids[idx]["name"]+"/"+self.Biraids[idx]["name"]+"_US_LR.dcm.npy")
        try:
            img_us = np.load(img_us)
        except:
            img_us = np.zeros([1000,1000,3],dtype=np.uint8)

        score = self.Biraids[idx]["score"]
        has_us = self.Biraids[idx]["us"]
        if (has_us == 0):
            img_us = np.zeros([1000,1000,3],dtype=np.uint8)

        mean = [0.485, 0.456, 0.406]
        stdv = [0.229, 0.224, 0.225]

        if self.transform:
            norm = []
            try:
                pil1 = transforms.ToPILImage()(np.array(img_us)) #Because the US image is 3-channel
                #pil2 = transforms.ToPILImage('L')(np.uint8(image[1]))
                #pil3 = transforms.ToPILImage('L')(np.uint8(image[2]))
                newPil = self.transform(pil1)
                #newPil2 = self.transform(pil2)
                #newPil3 = self.transform(pil3)
                #newPil = torch.cat((newPil1,newPil2,newPil3))
                norm=transforms.Normalize(mean=mean, std=stdv)(newPil)
            except:
                pil = transforms.ToPILImage()(np.uint8(img_us))
                newPil = self.transform(pil)
                duplicate= newPil.repeat(3, 1, 1)
                norm=transforms.Normalize(mean=mean, std=stdv)(duplicate)
        #final_image = transforms,concat(0,[norm_lcc,norm_rcc,norm_rmlo,norm_lmlo,norm])
            newTuple = (norm, score)
            return newTuple
        return (0,0)

def demo(data, save, lr, wd, shuffle, dropout, random_crop, n_epochs, depth=40, growth_rate=12, efficient=True, valid_size=None, batch_size=60, seed=None):
    """
    A demo to show off training of efficient DenseNets.
    Trains and evaluates a DenseNet-BC on CIFAR-10.

    Args:
        data (str) - path to directory where data should be loaded from/downloaded
            (default $DATA_DIR)
        save (str) - path to save the model to (default /tmp)

        depth (int) - depth of the network (number of convolution layers) (default 40)
        growth_rate (int) - number of features added per DenseNet layer (default 12)
        efficient (bool) - use the memory efficient implementation? (default True)

        valid_size (int) - size of validation set
        n_epochs (int) - number of epochs for training (default 300)
        batch_size (int) - size of minibatch (default 256)
        seed (int) - manually set the random seed (default None)
    """
    #torch.cuda.empty_cache()
    # Get densenet configuration
    if (depth - 4) % 3:
        raise Exception('Invalid depth')
    block_config = [(depth - 4) // 6 for _ in range(3)]

    # Data transforms
    #mean = [0.5071, 0.4867, 0.4408]
    #stdv = [0.2675, 0.2565, 0.2761]

    # mean = [0.485, 0.456, 0.406]
    # stdv = [0.229, 0.224, 0.225]
    train_transforms = transforms.Compose([
        transforms.Resize((256,256)),
        transforms.ToTensor(),
    ])
    test_transforms = transforms.Compose([
        transforms.Resize((256,256)),
        transforms.ToTensor(),
    ])

    crop_transforms = transforms.Compose([
        transforms.RandomCrop(300,pad_if_needed=False,fill=0,padding_mode='constant'),
        transforms.Resize((256,256)),
        transforms.ToTensor(),
    ])

    # Datasets
    bi_raids_l = LoadBiRaids("mamo_patients.csv", 1)  ## Left Breasts  = 1
    bi_raids_r = LoadBiRaids("mamo_patients.csv",0)   ## Right Breasts = 0
    bi_raids = bi_raids_l + bi_raids_r
    #random.shuffle(bi_raids)
    #batch_size = int(len(bi_raids)/3)
    train_biraids = bi_raids[0:int(len(bi_raids)*0.60)]
    valid_biraids = bi_raids[int(len(bi_raids)*0.60):int(len(bi_raids)*0.80)]
    test_biraids = bi_raids[int(len(bi_raids)*0.80):]
    print('Train Data: ', len(train_biraids), 'Test Data: ', len(test_biraids), 'Validation Data: ', len(test_biraids))
    train_set = LoadDataset(train_biraids, train_transforms)
    if random_crop:
        for i in range(4):
            train_set += LoadDataset(train_biraids, crop_transforms)
    #random.shuffle(train_set)

    #test_set = LoadDataset(data, train=False, transform=test_transforms, download=False)

    test_set = LoadDataset(test_biraids, train_transforms)

    if valid_size:
        valid_set = LoadDataset(train_biraids, train_transforms)
        indices = torch.randperm(len(train_set))
        train_indices = indices[:len(indices) - valid_size]
        valid_indices = indices[len(indices) - valid_size:]
        train_set = torch.utils.data.Subset(train_set, train_indices)
        valid_set = torch.utils.data.Subset(valid_set, valid_indices)
    else:
        valid_set = None

    valid_set = LoadDataset(valid_biraids, train_transforms)

    ## Check the balance of classes in the dataset
    #cancer = 0
    #no_cancer =0
    #for i in range(len(valid_set)):
    #    if (valid_set[i][1] == 1):
    #        cancer += 1
    #    else:
    #        no_cancer += 1
    #print("Cancer = ", cancer, " No Cancer = ", no_cancer)

    model = torch.hub.load('pytorch/vision', 'densenet121', pretrained=False, drop_rate=dropout,num_classes=2)

    print(save)
    # Make save directory
    if not os.path.exists(save):
        os.makedirs(save)
    if not os.path.isdir(save):
        raise Exception('%s is not a dir' % save)

    # Train the model
    train(model=model, train_set=train_set, valid_set=valid_set, test_set=test_set, save=save, n_epochs=n_epochs, batch_size=batch_size, seed=seed, lr=lr, wd=wd, shuffle=shuffle)
    print('Done!')


"""
A demo to show off training of efficient DenseNets.
Trains and evaluates a DenseNet-BC on CIFAR-10.

Try out the efficient DenseNet implementation:
python demo.py --efficient True --data <path_to_data_dir> --save <path_to_save_dir>

Try out the naive DenseNet implementation:
python demo.py --efficient False --data <path_to_data_dir> --save <path_to_save_dir>

Other args:
    --depth (int) - depth of the network (number of convolution layers) (default 40)
    --growth_rate (int) - number of features added per DenseNet layer (default 12)
    --n_epochs (int) - number of epochs for training (default 300)
    --batch_size (int) - size of minibatch (default 256)
    --seed (int) - manually set the random seed (default None)
"""
if __name__ == '__main__':
    fire.Fire(demo)
