#######

IMPLEMENTING MULTIMODAL DENSENET:
Author: Sam Sukeerth Karem

#######
Dataset:

1. All the images for US, MLO, and CC modalities are present in the '../dataset' folder. 

2. These images were converted into numpy format from dicom format using the 'Segmentation.ipynb' jupyter notebook. The code in the notebook should be self explantory with necessary comments.

3. The pathology report for the patients is found in the mamo_patients.csv file. This is mapped with the dataset in the code for running models.

4. Some Images of the dataset can also be found in the '../Dataset\ Images' folder


UNI MODAL:

1. Unimodal code for CC is found in demoCC.py. It's implemented using the shell script runcc.sh. The Hyperparameters of the model can be modified in the script rather than modifying them in the code

2. Unimodal code for MLO is found in demoMlo.py. It's implemented using the shell script runcc.sh. The Hyperparameters of the model can be modified in the script rather than modifying them in the code

3. Unimodal code for US is found in demoUS.py. It's implemented using the shell script runcc.sh. The Hyperparameters of the model can be modified in the script rather than modifying them in the code

MULTI_MODAL:

1. The multimodal densenet named 'Condensenet' is found in the './models' folder. 

2. This model is used is running the multimodal code found in demo.py.

3. It's implemented using the sheel script run.sh. Same as the Unimodal hyperparameters can be modified here.


