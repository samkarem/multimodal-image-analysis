import fire as fire
import os
import time
import random
import pandas as pd
import torch
from sklearn import metrics
import numpy as np
import csv
import torch.nn as nn
from skimage import exposure
from skimage.morphology import disk
from skimage.filters import rank
import torchvision.models as models
import torch.utils.data as data
from torchvision import datasets, transforms
import math
import torch.nn.functional as F
import torch.utils.checkpoint as cp
from collections import OrderedDict
import torch.nn.functional as F

def _bn_function_factory(norm, relu, conv):
    def bn_function(*inputs):
        concated_features = torch.cat(inputs, 1)
        bottleneck_output = conv(relu(norm(concated_features)))
        return bottleneck_output

    return bn_function


class _DenseLayer(nn.Module):
    def __init__(self, num_input_features, growth_rate, bn_size, drop_rate, efficient=False):
        super(_DenseLayer, self).__init__()
        self.add_module('norm1', nn.BatchNorm2d(num_input_features)),
        self.add_module('relu1', nn.ReLU(inplace=True)),
        self.add_module('conv1', nn.Conv2d(num_input_features, bn_size * growth_rate,
                        kernel_size=1, stride=1, bias=False)),
        self.add_module('norm2', nn.BatchNorm2d(bn_size * growth_rate)),
        self.add_module('relu2', nn.ReLU(inplace=True)),
        self.add_module('conv2', nn.Conv2d(bn_size * growth_rate, growth_rate,
                        kernel_size=3, stride=1, padding=1, bias=False)),
        self.drop_rate = drop_rate
        self.efficient = efficient

    def forward(self, *prev_features):
        bn_function = _bn_function_factory(self.norm1, self.relu1, self.conv1)
        if self.efficient and any(prev_feature.requires_grad for prev_feature in prev_features):
            bottleneck_output = cp.checkpoint(bn_function, *prev_features)
        else:
            bottleneck_output = bn_function(*prev_features)
        new_features = self.conv2(self.relu2(self.norm2(bottleneck_output)))
        if self.drop_rate > 0:
            new_features = F.dropout(new_features, p=self.drop_rate, training=self.training)
        return new_features


class _Transition(nn.Sequential):
    def __init__(self, num_input_features, num_output_features):
        super(_Transition, self).__init__()
        self.add_module('norm', nn.BatchNorm2d(num_input_features))
        self.add_module('relu', nn.ReLU(inplace=True))
        self.add_module('conv', nn.Conv2d(num_input_features, num_output_features,
                                          kernel_size=1, stride=1, bias=False))
        self.add_module('pool', nn.AvgPool2d(kernel_size=2, stride=2))
    def search(self):
        return "_Transition"

class _DenseBlock(nn.Module):
    def __init__(self, num_layers, num_input_features, bn_size, growth_rate, drop_rate, efficient=False):
        super(_DenseBlock, self).__init__()
        for i in range(num_layers):
            layer = _DenseLayer(
                num_input_features + i * growth_rate,
                growth_rate=growth_rate,
                bn_size=bn_size,
                drop_rate=drop_rate,
                efficient=efficient,
            )
            self.add_module('denselayer%d' % (i + 1), layer)

    def get_layers(self):
        layers = []
        for name, layer in self.named_children():
            layers.append(layer)
        return layers


    def forward(self, init_features):
        features = [init_features]
        for name, layer in self.named_children():
            new_features = layer(*features)
            features.append(new_features)
        return features


class _ConDenseBlock(nn.Module):
    def __init__(self, num_layers, num_input_features, bn_size, growth_rate, drop_rate, efficient=False):
        super(_ConDenseBlock, self).__init__()
        for i in range(num_layers):
            layer = _DenseLayer(
                num_input_features + i * growth_rate,
                growth_rate=growth_rate,
                bn_size=bn_size,
                drop_rate=drop_rate,
                efficient=efficient,
            )
            self.add_module('denselayer%d' % (i + 1), layer)
    def search(self):
        return "_ConDenseBlock"

    def get_layers(self):
        layers = []
        for name, layer in self.named_children():
            layers.append(layer)
        return layers

    def forward(self, init_features):
        features = [init_features]
        #features_second = [second]
        #features_third = [third]

        # Get Denseblocks from seond and third modality using thieir module names
        #features_second = [getattr(self.condensenet.features_second,second)]
        #features_third = [getattr(self.condensenet.features_third, third)]
        for name, layer in self.named_children():
            #print(layer)
            new_features = layer(*features)
            #new_features_second = layer(*features_second)
            #new_features_third = layer(*features_third)

            #features.append(new_features)
            #Append features from Second and third modality into First
            #features.append(new_features_second)
            #new_features += (new_features_third + new_features_second)
            features.append(new_features)
            #features_second.append(new_features_second)
            #features_third.append(new_features_third)
        return torch.cat(features,1)


class Condensenet(nn.Module):

    def __init__(self, growth_rate=32, block_config=(6, 12, 24, 16), compression=0.5,
                 num_init_features=64, bn_size=4, drop_rate=0,
                 num_classes=2, small_inputs=False, efficient=True):

        super(Condensenet, self).__init__()
        # block_config=(16, 16, 16)
        assert 0 < compression <= 1, 'compression of densenet should be between 0 and 1'
         # Initialization

        self.avgpool_size = 8 if small_inputs else 7
       # First convolution
        if small_inputs:
            self.features_main = nn.Sequential(OrderedDict([
                ('conv0', nn.Conv2d(3, num_init_features, kernel_size=3, stride=1, padding=1, bias=False)),
            ]))

            self.features_second = nn.Sequential(OrderedDict([
                ('conv0', nn.Conv2d(3, num_init_features, kernel_size=3, stride=1, padding=1, bias=False)),
            ]))
            self.features_third = nn.Sequential(OrderedDict([
                ('conv0', nn.Conv2d(3, num_init_features, kernel_size=3, stride=1, padding=1, bias=False)),
            ]))
        else:
            self.features_main = nn.Sequential(OrderedDict([
                ('conv0main', nn.Conv2d(3, num_init_features, kernel_size=7, stride=2, padding=3, bias=False)),
            ]))
            self.features_main.add_module('normm0', nn.BatchNorm2d(num_init_features))
            self.features_main.add_module('relum0', nn.ReLU(inplace=True))
            self.features_main.add_module('poolm0', nn.MaxPool2d(kernel_size=3, stride=2, padding=1,
                                                           ceil_mode=False))
            self.features_second = nn.Sequential(OrderedDict([
                ('conv0second', nn.Conv2d(3, num_init_features, kernel_size=7, stride=2, padding=3, bias=False)),
            ]))
            self.features_second.add_module('norms0', nn.BatchNorm2d(num_init_features))
            self.features_second.add_module('relus0', nn.ReLU(inplace=True))
            self.features_second.add_module('pools0', nn.MaxPool2d(kernel_size=3, stride=2, padding=1,
                                                           ceil_mode=False))
            self.features_third = nn.Sequential(OrderedDict([
                ('conv0third', nn.Conv2d(3, num_init_features, kernel_size=7, stride=2, padding=3, bias=False)),
            ]))
            self.features_third.add_module('normt0', nn.BatchNorm2d(num_init_features))
            self.features_third.add_module('relut0', nn.ReLU(inplace=True))
            self.features_third.add_module('poolt0', nn.MaxPool2d(kernel_size=3, stride=2, padding=1,
                                                           ceil_mode=False))


        self.denseblock_main = nn.ModuleList()
        self.denseblock_second = nn.ModuleList()
        self.denseblock_third = nn.ModuleList()
        # Dense blocks with connections between the three 3 modalities
        num_features = num_init_features
        for i, num_layers in enumerate(block_config):
            block = _ConDenseBlock(
                num_layers=num_layers,
                num_input_features=num_features,
                bn_size=bn_size,
                growth_rate=growth_rate,
                drop_rate=drop_rate,
                efficient=efficient
            )
            block_second = _DenseBlock(
                num_layers=num_layers,
                num_input_features=num_features,
                bn_size=bn_size,
                growth_rate=growth_rate,
                drop_rate=drop_rate,
                efficient=efficient,
            )
            block_third = _DenseBlock(
                num_layers=num_layers,
                num_input_features=num_features,
                bn_size=bn_size,
                growth_rate=growth_rate,
                drop_rate=drop_rate,
                efficient=efficient,
            )

            self.denseblock_second.append(block_second)
            self.denseblock_third.append(block_third)
            #cc,mlo,us = block(cc,mlo,us)
            self.denseblock_main.append(block)

            num_features = num_features + num_layers * growth_rate
            if i != len(block_config) - 1:
                trans = _Transition(num_input_features=num_features,
                                    num_output_features=int(num_features * compression))
                self.denseblock_main.append(trans)
                self.denseblock_second.append(trans)
                self.denseblock_third.append(trans)
                num_features = int(num_features * compression)
        self.norm = nn.Sequential(OrderedDict([('norm_final_main', nn.BatchNorm2d(num_features))]))
        #self.norm.add_module('norm_final_main', nn.BatchNorm2d(num_features))

        # Linear layer
        self.classifier = nn.Linear(num_features, num_classes)

        for name, param in self.named_parameters():
            if 'conv' in name and 'weight' in name:
                n = param.size(0) * param.size(2) * param.size(3)
                param.data.normal_().mul_(math.sqrt(2. / n))
            elif 'norm' in name and 'weight' in name:
                param.data.fill_(1)
            elif 'norm' in name and 'bias' in name:
                param.data.fill_(0)
            elif 'classifier' in name and 'bias' in name:
                param.data.fill_(0)

    def forward(self, x,y,z):
        second = self.features_second(y)
        third = self.features_third(z)
        main = self.features_main(x)
        length = len(self.denseblock_main)
        for i in range(length):
            #second = self.denseblock_second[i](second)
            #third = self.denseblock_third[i](third)
            #print("This is i ",i)
            if self.denseblock_main[i].search() == "_ConDenseBlock":
                main_layer = self.denseblock_main[i].get_layers()
                second_layer = self.denseblock_second[i].get_layers()
                third_layer = self.denseblock_third[i].get_layers()
                main = [main]
                second = [second]
                third = [third]

                for j in range(len(main_layer)):
                    new_main = main_layer[j](*main)
                    new_second = second_layer[j](*second)
                    new_third = third_layer[j](*third)
                    new_main += (new_second + new_third)
                    main.append(new_main)
                    #main.append(new_second)
                    #main.append(new_third)
                    second.append(new_second)
                    third.append(new_third)
                    #print("Length of main: ", len(main), "leng of second: ", len(second))

                main = torch.cat(main,1)
                second = torch.cat(second,1)
                third =  torch.cat(third,1)
            else:
                main = self.denseblock_main[i](main)
                second = self.denseblock_second[i](second)
                third = self.denseblock_third[i](third)
        #second = self.denseblock_second(second)
        #third = self.denseblock_third(third)
        #main = self.denseblock_main(main)
        main = self.norm(main)
        out = F.relu(main,inplace=True)
        #out = F.avg_pool2d(out, kernel_size=self.avgpool_size).view(main.size(0), -1)
        out = F.adaptive_avg_pool2d(out,(1,1))
        out = torch.flatten(out,1)
        out = self.classifier(out)

        return out

